@extends('adminlte.master')
 
@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('posts.index') }}"> Back</a>
        </div>
    </div>
</div>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<form action="{{ route('posts.store') }}" method="POST">
    @csrf
 
    <div class="form-group">
        <label for="exampleInputEmail1">Judul</label>
        <input type="text" class="form-control" name="judul"  placeholder="Enter Judul">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Ringkasan</label>
        <input type="text" class="form-control" name="ringkasan"  placeholder="Enter Ringkasan">
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Tahun</label>
        <input type="text" class="form-control" name="tahun"  placeholder="Enter Tahun">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Poster</label>
        <input type="text" class="form-control" name="poster"  placeholder="Enter Poster">
    </div>


        <button type="submit" class="btn btn-primary">Submit</button>
 
</form>
@endsection